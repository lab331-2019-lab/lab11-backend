package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.LecturerAnotherService;
import se331.lab.rest.service.StudentAnotherService;

@Controller
@Slf4j
public class StudentAnotherController {
    @Autowired
    StudentAnotherService studentAnotherService;
    @GetMapping("/studentNameContain/{partOfName}")
    public ResponseEntity getLectureBySurname(@PathVariable String partOfName) {
        log.info("the controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentAnotherService.getStudentByNameContins(partOfName)));
    }
    @GetMapping("/studentAdvisorWhoseNameIs/{AdvisorName}")
    public ResponseEntity getLectureBySurnames(@PathVariable String AdvisorName) {
        log.info("the controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentAnotherService.getStudentWhoseAdvisorNameIs(AdvisorName)));
    }
}
