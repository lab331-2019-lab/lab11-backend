package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.dao.StudentAnotherDao;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;
@Service
public class CourseAnotherServiceImp implements CourseAnotherService {
    @Autowired
    CourseAnotherDao courseAnotherDao;
    @Override
    public List<Course> getCourseWhichStudentEnrolledMoreThan(int number) {
        List<Course> courses = courseAnotherDao.getAllCourse();
        List<Course> output = new ArrayList<>();
        for (Course course: courses) {
            double num = course.getStudents().size();
            if (num >= number){
                output.add(course);
            }
        }
        return output;
    }

}
